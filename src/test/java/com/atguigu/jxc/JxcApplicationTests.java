package com.atguigu.jxc;

import com.alibaba.fastjson2.JSON;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import springfox.documentation.spring.web.json.Json;

import java.io.FileNotFoundException;
import java.util.HashMap;

@SpringBootTest
public class JxcApplicationTests {


	//在 @SpringbootTest 测试类中。对持久层进行更新操作失效，数据库中结果未更新。
	//原因：开启了事务。在@SpringBootTest测试类中，如果加了事务控制@Transactional，则进行任何增删改，都默认进行回滚，所以 DML操作都会回滚。
	//详细原因在 TransactionContext 类中可以找到（org.springframework.test.context.transaction.TransactionContext）
	//INFO  o.s.t.c.t.TransactionContext - Rolled back transaction for test

	@Autowired
	ServiceB serviceB;


	@Test
	public void TestException() throws FileNotFoundException {
//		serviceB.exception1();

		serviceB.exception2();

//		serviceB.exception3();
	}

	//异常捕获导致事务失效



	@Test
	public void clientHttp() {
//		serviceB.exception1();

//		serviceB.exception2();
		String url = "https://area9-win.pospal.cn:443/pospal-api2/openapi/v1/productOpenApi/queryProductCategoryPages";


		HashMap map = new HashMap<>();
		HashMap map1 = new HashMap<>();
		map.put("appId","4436939EE5757EBC9AF59921004FB124");
		map1.put("parameterType",1);
		map1.put("parameterValue",1);
		map.put("postBackParameter",map1);
		String s = JSON.toJSONString(map);
		System.out.println("s = " + s);

		String s1 = HttpClientUtil2.doPost(url, s);
		System.out.println("s1 = " + s1);


//		serviceB.exception3();
	}


}

