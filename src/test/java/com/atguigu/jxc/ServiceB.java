package com.atguigu.jxc;

import com.atguigu.jxc.dao.GoodsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

@Service
public class ServiceB {

    @Autowired
    private GoodsDao goodsDao;

    @Transactional
    public void exception1() {
        try {
            Integer integer = goodsDao.updateGoodsByGoodsId(10, 1);
            System.out.println("integer = " + integer);
            int test = 10 / 0;
            Integer integer1 = goodsDao.updateGoodsByGoodsId2(10, 2);
            System.out.println("integer1 = " + integer1);
        } catch (Exception e) {
			e.printStackTrace();
            //抛出异常 事务生效
			throw new RuntimeException(e);
        }
    }


    @Transactional(rollbackFor = Exception.class)
//    @Transactional
    public void exception2() throws FileNotFoundException {

        goodsDao.updateGoodsByGoodsId(10, 1);

        new FileInputStream("/home/log/cjy.log");

        goodsDao.updateGoodsByGoodsId2(10, 2);
    }


    @Transactional(rollbackFor = Exception.class)
//    @Transactional
    void exception3() throws FileNotFoundException {

        goodsDao.updateGoodsByGoodsId(10, 1);

        new FileInputStream("/home/log/cjy.log");

        goodsDao.updateGoodsByGoodsId2(10, 2);
    }


}
